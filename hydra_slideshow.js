
Drupal.behaviors.hydraSlideshow = function() {
  Drupal.hydraPlayer.onLoad = function() {
    // TODO: Theme
    var html = '<ul id="playlist"><li class="${itemClass}"><img src="${thumbnail}" alt="Slideshow thumbnail"><strong>${title}</strong></li></ul>';
    if (pl.length == 1) {
      $.each(pl[0], function(key, val) {	
				if (!$.isFunction(val)) {
					html = html.replace("$\{" +key+ "\}", val);
				}
			});
    }
    $('#playlist-wrapper').removeClass('waiting').html(html);
    Drupal.hydraPlayer.player.setPlaylist(pl);
    Drupal.hydraPlayer.player.playlist("#playlist", {loop: true});
    $("#playlist").jcarousel({
      initCallback: function(jc, status) {
        // Because IE fires the jcarousel resize callback when reloading the 
        // playlist, and it breaks everything, we remove it.
        jc.funcResize = null;       
        carousel = jc;
      },
      vertical: true
    });
  };
  
  Drupal.hydraPlayer.onError = function(code, message) {
    if ((code == 200) || (code == 201) || (code == 303)) {
      var ci = Drupal.hydraPlayer.player.getClip().index;
      var pi = Drupal.hydraPlayer.player.getPlaylist().length;
      Drupal.hydraPlayer.player.play((ci == (pi-1)) ? 0 : (ci+1));
    }
  };
  
  Drupal.hydraPlayer.onBufferFull = function(clip) {
    carousel.scroll(parseInt(clip.index));
    this.getPlugin("content").setHtml("<p align='center'><a href='" + Drupal.hydraPlayer.player.getClip().clickUrl + "'>Leggi tutto</a></p>"); 
  };
  
  Drupal.hydraPlayer.onFinish = function(clip) {
    if (clip.index == (Drupal.hydraPlayer.player.getPlaylist().length - 1)) {
      Drupal.hydraPlayer.player.stop();
      if ($('.block-hydra_slideshow ul.pager').length > 0) {
        if ($('.block-hydra_slideshow li.pager-next.last a').length > 0) {
          $('.block-hydra_slideshow li.pager-next.last a').click();
        }
      } else {
        setTimeout(function() {
          var ci = Drupal.hydraPlayer.player.getClip().index;
          var pi = Drupal.hydraPlayer.player.getPlaylist().length;
          Drupal.hydraPlayer.player.play((ci == (pi-1)) ? 0 : (ci+1));    
        }, 100);
      }
    } 
  };
}
