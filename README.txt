
/**
 * @file
 * README file for Hydra Slideshow.
 * @ingroup hydra_slideshow
 */ 

 
Hydra Slideshow is an helper module for building a slideshow using Views,
Flowplayer, jCarousel and ImageCache.
