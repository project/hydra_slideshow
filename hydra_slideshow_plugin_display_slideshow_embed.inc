<?php

/**
 * The plugin that handles a slideshow embed.
 */
class hydra_slideshow_plugin_display_slideshow_embed extends views_plugin_display {
  function option_definition() {
    $options = parent::option_definition();
    $options['slideshow_header'] = array('default' => '', 'translatable' => TRUE);
    $options['slideshow_footer'] = array('default' => '', 'translatable' => TRUE);
    $options['slideshow_hint'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  /**
   * Provide the summary for page options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);

    $categories['slideshow_embed'] = array(
      'title' => t('Embed settings'),
    );
    
    $title = $this->get_option('slideshow_header');
    if (empty($title)) {
      $title = t('No title');
    }
    $title = strip_tags($title);
    $options['slideshow_header'] = array(
      'category' => 'slideshow_embed',
      'title' => t('Header'),
      'value' => $title,
    );
    
    $desc = $this->get_option('slideshow_footer');
    if (empty($desc)) {
      $desc = t('No description');
    }
    $desc = strip_tags($desc);
    $options['slideshow_footer'] = array(
      'category' => 'slideshow_embed',
      'title' => t('Footer'),
      'value' => $desc,
    );    
    
    $hint = $this->get_option('slideshow_hint');
    if (empty($hint)) {
      $hint = t('No hint');
    }
    $hint = strip_tags($hint);
    $options['slideshow_hint'] = array(
      'category' => 'slideshow_embed',
      'title' => t('Hint'),
      'value' => $hint,
    );    
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'slideshow_header':
        $form['slideshow_header'] = array(
          '#title' => t('Header'),
          '#type' => 'textarea',
          '#default_value' => $this->get_option('slideshow_header'),
          '#description' => t("The text that will appear in the embed's header."),
        );
        break;
        
      case 'slideshow_footer':
        $form['slideshow_footer'] = array(
          '#title' => t('Footer'),
          '#type' => 'textarea',
          '#default_value' => $this->get_option('slideshow_footer'),
          '#description' => t("The text that will appear in the embed's footer."),
        );
        break;     
        
      case 'slideshow_hint':
        $form['slideshow_hint'] = array(
          '#title' => t('Hint'),
          '#type' => 'textarea',
          '#default_value' => $this->get_option('slideshow_hint'),
          '#description' => t("The text that will appear in the embed popup."),
        );
        break;              
    }
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit($form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'slideshow_header':
      case 'slideshow_footer':
      case 'slideshow_hint':
        $this->set_option($form_state['section'], $form_state['values'][$form_state['section']]);
        break;
    }
  }
}

