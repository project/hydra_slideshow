<?php
/**
 * @file hydra_slideshow.inc
 * Private functions.
 */

/**
 * Return an image path for a theme or fallback to default.
 *
 * @param $image
 *   The name of the image file.
 * @param $theme
 *   The name of the theme.
 * @param $default
 *   If given, force the function to provide a default image when the required
 *   doesn't exist.
 *
 * @return
 *   The path to an image or nothing.
 */
function _hydra_slideshow_theme_image($image, $theme, $default = FALSE) {
  global $base_path;
  $path = drupal_get_path('module','hydra_slideshow') . '/themes';
  $theme_image = $path . '/' . $theme . '/images/' . $image . '.png';
  if (file_exists($_SERVER['DOCUMENT_ROOT'] . $base_path . $theme_image)) {
    return $theme_image;
  }
  elseif ($default) {
    return $path . '/default/images/' . $image . '.png';
  }
  return '';
}

/**
 * Return image and thumbnail paths generated by imagecache.
 *
 * We cannot use theme('imagecache') so we need to take care of image 
 * generation if needed.
 * Some code taken from _imagecache_cache().
 *
 * @param $node
 *   The node from which to pull the imagecache paths.
 *
 * @return
 *   An array containing image and thumbnail paths.
 */
function _hydra_slideshow_get_imagecache_images($node) {
  global $base_url, $base_path;
  static $preset = array();
  $images = array();
  
  // Load imagecache preset
  if (!$preset) {
    if (!$preset = imagecache_preset_by_name('hydra_slideshow')) {
      // Send a 404 if we don't know of a preset.
      header("HTTP/1.0 404 Not Found");
      exit;
    }  
  }
  $src = $node->images['_original'];      
  $dst = imagecache_create_path($preset['presetname'], $src);
  $image = '';
  // Return imagecached paths early if image exists
  if (is_file($dst)) {      
    $image = str_replace($base_url, '', imagecache_create_url('hydra_slideshow', $src));
    $image = imagecache_create_url('hydra_slideshow', $src);
  }
  // Otherwise we need to create derivative
  else {
    $original = str_replace($base_url, '', file_create_url($src));
    // First check for original image
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . $base_path . $original)) {
      // Original exists, try to create derivative
      if (imagecache_build_derivative($preset['actions'], $src, $dst)) {
        $image = str_replace($base_url, '', imagecache_create_url('hydra_slideshow', $src));
      }
      // Failed, fallback to original image
      else {
        $image = $original;
      }
    } 
  }
  if ($image && preg_match("#\.(jp[e]?g)|(gif)|(png)|(bmp)$#", strtolower($image))) {
    $images['thumb'] = imagecache_create_url('hydra_slideshow_thumb', $src);          
    $images['url']   = $image;
  }
  return $images;
}

/**
 * Format the playlist item according to the node type.
 *
 * @param $row
 *   A view row.
 * @param $view
 *   The view responsible of the row generation.
 *
 * @return
 *   An array containing the playlist item.
 */
function _hydra_slideshow_format_playlist_item($row, $view) {
  global $base_url, $base_path;
  $theme = $view->style_plugin->options['slideshow_theme'];
  $no_image = _hydra_slideshow_theme_image('empty', $theme, TRUE);
  
  $node = node_load($row->nid);
  
  $overlay = array();
  foreach ($view->style_plugin->row_plugin->options['overlay'] as $option) {
    if ($text = html_entity_decode(trim(strip_tags(str_replace("\r\n", '', $row->$option))), ENT_NOQUOTES, 'UTF-8')) {
      $overlay[] = $text;
    }
  } 
  $item = array(
    'title'    => html_entity_decode(truncate_utf8(check_plain($node->title), 50, TRUE, TRUE), ENT_NOQUOTES, 'UTF-8'),
    'link'     => url("node/$node->nid"),  
    'duration' => $view->style_options['slide_duration'],
    'class'    => '',
    'url'      => "{$base_path}{$no_image}",
    'thumb'    => "{$base_url}/{$no_image}",
    'overlay'  => join(' - ', $overlay),
  );    
  $module = node_get_types('module', $node);
  if ($result = module_invoke($module, 'slideshow_item', $node)) {
    $item = array_merge($item, $result);
  }
  else {
    switch ($node->type) {
      case 'image':
        if ($images = _hydra_slideshow_get_imagecache_images($node)) {
          $item['thumb'] = $images['thumb'];
          $item['url']   = $images['url'];
        }
        break;
                
      default:
        $title = html_entity_decode($node->title, ENT_NOQUOTES, 'UTF-8');
        $html = '<p><b><font size="16">' . $title . '</font></b></p><br />';
        $teaser = html_entity_decode($node->teaser, ENT_NOQUOTES, 'UTF-8');
        $teaser = str_replace(array('<strong>', '</strong>'), array('<b>', '</b>'), $teaser);
        $teaser = str_replace(array('<em>', '</em>'), array('<i>', '</i>'), $teaser);
        $html .= $teaser;
      $item['url'] = $base_url . '/' . drupal_get_path('module','hydra_slideshow') . '/textslide.swf?html_code=' . urlencode($html);
        break;        
    }
  }
  return $item;
}

/**
 * A very simple parser for css files.
 *
 * @param $filename
 *   The name of the file, with path relative to the document root.
 *
 * @return
 *   The css structure as an array. 
 */
function _hydra_slideshow_get_embed_css($filename) {
  global $base_path;
  // Read css file
  $file = $_SERVER['DOCUMENT_ROOT'] . $base_path . $filename;
  $handle = fopen($file, 'r');
  $css = fread($handle, filesize($file));
  fclose($handle);
  // Parse (simple) css
  $css = preg_replace("/\/\*(.*)?\*\//Usi", "", $css);
  $declarations = explode("}", $css);
  $items = array();
  if (count($declarations) > 0) {
    foreach ($declarations as $declaration) {
      list ($keystr, $codestr) = explode("{", $declaration);
      $selectors = explode(",", trim($keystr));
      if (count($selectors) > 0) {
        foreach ($selectors as $selector) {
          if (strlen($selector) > 0) {
            $selector = str_replace("\n", "", $selector);
            $selector = str_replace("\\", "", $selector);
            $items[$selector] = array();
            $propdecs = split(';', trim($codestr));            
            foreach ($propdecs as $propdec) {
              if (trim($propdec)) {
                list($property, $value) = split(':', $propdec);
                $items[$selector][trim($property)] = trim($value);
              }
            }
          }
        }
      }
    }
  }
  return $items;
}

/**
 * Pull the url from a css declaration.
 */
function _hydra_slideshow_css_image_url($property) {
  preg_match("/url\((.*)\)/", $property, $matches);
  return $matches[1];
}
/**
 * Generate the player configuration form view's display settngs.
 *
 * @param $view_name
 *   The name of the view
 * @param $display_name
 *   The internal name of the display
 *
 * @return
 *   An associative array containing the configuration
 */
function _hydra_slideshow_player_config($view_name, $display_name) {
  global $base_url;  
  $config = array();
  if ($view = views_get_view($view_name)) {
    $view->set_display($display_name);
    $display = $view->display_handler;
    $style_options = $display->default_display->options['style_options'];
    $width = (int)$style_options['player_width'];
    $height = (int)$style_options['player_height'];
    $config = array(
      'clip' => array(
        'autoBuffering' => TRUE,
        'autoPlay'      => TRUE,
        'duration'      => (int)$style_options['slide_duration'],
      ),    
      'swf_plugins' => array(
        'controls'  => NULL,
        'overlay'   => array(
          'top'     => "90%",
          'left'    => 5,
          'width'   => $width - 100,        
          'height'  => 40,        
          'zIndex'  => 2,        
        ),
        'content'   => array(
          'bottom'  => 10,
          'right'   => 10, 
          'width'   => 80, 
          'height'  => 30,
          'opacity' => 1,
          'style'   => array(
            'p'     => array('fontSize' => 12),
            'a'     => array('textDecoration' => 'underline'),
          ),
          'background' => '#B24834',
          'border'     => '0',
          'zIndex'     => 3,        
        ),      
      )
    );    
    if ($display->display->display_plugin == 'slideshow') {
      $config['js_plugins'] = array(
        'playlist' => array(),
      );
    }
    elseif ($display->display->display_plugin == 'slideshow_embed') {
      $theme = $style_options['slideshow_theme'];
      $path = drupal_get_path('module','hydra_slideshow');
      $theme_css = $path . '/themes/' . $theme . '/embed.css';   
      $css = _hydra_slideshow_get_embed_css($theme_css);
      $config['screen'] = array(
        'width'  => $width, 
        'height' => $height,
        'top'    => 20,
      );
      $header = urlencode($display->options['slideshow_header']);
      $text_color = str_replace('#', '0x', $css['#canvas']['color']);
      $config['canvas'] = array(
        'backgroundColor' => $css['#canvas']['background-color'], 
        'backgroundImage' => "url({$base_url}/{$path}/embed_bg.swf?text={$header}&color={$text_color})", 
      );    
      
      $config['swf_plugins']['overlay'] = array(
        'bottom'          => 40,
        'width'           => '100%',
        'height'          => 20,
        'left'            => 4,
        'backgroundColor' => '#333333',
        'borderRadius'    => 0,
        'opacity'         => 1,
        'fontSize'        => 11,
      );
      $logo = '';
      if ($logo_url = _hydra_slideshow_css_image_url($css['#logo']['background-image'])) {
        $logo_url = $base_url . '/' . $path . '/themes/' . $theme . '/' . $logo_url;
        $logo = '<a href="' . $base_url . '" class="logo_link"><img src="' . $logo_url . '" vspace="0" hspace="0" /></a>';
      }
      $config['swf_plugins']['content'] = array(
        'bottom'       => 0,
        'height'       => 40,
        'borderRadius' => 0,
        'width'        => '100%',
        'opacity'      => 1,        
        'stylesheet'   => $base_url . '/' . $theme_css,
        'background'   => $css['#footer']['background-color'],
        'border'       => '0',
        'html'         => '<p>'. $logo . $display->options['slideshow_footer'] . '</p>',
      );    
    }
  }
  return $config;
}

/**
 * Get a list of themes through discovery of the themes directory
 */
function _hydra_slideshow_get_themes() {
  static $themes = array();
  if (count($themes) > 0) {
    return $themes;
  }
  $path = drupal_get_path('module', 'hydra_slideshow') . '/themes';
  $dir_handle = @opendir($path) or die("Unable to open $path");
  while ($file = readdir($dir_handle)) {
    if (is_dir($path . '/' . $file) && !in_array($file, array('.', '..', '.svn', 'CVS'))) {
      $themes[$file] = $file;
    }
  }
  closedir($dir_handle);
  asort($themes);
  return $themes;
}
