<?php
// $I$

/**
 *  @file
 *  Defines slideshow view style plugins.
 */

/**
 * Slideshow View style plugins. Implementation of hook_views_plugins()
 */
function hydra_slideshow_views_plugins() {
  return array(
    'display' => array(
      'slideshow' => array(
        'title' => t('Slideshow'),
        'help' => t('Slideshow display.'),
        'handler' => 'hydra_slideshow_plugin_display_slideshow',
        'use ajax' => TRUE,
        'use pager' => TRUE,        
        'theme' => 'views_view',
      ),      
      'slideshow_embed' => array(
        'title' => t('Slideshow embed'),
        'help' => t('Display for embedded slideshow.'),
        'handler' => 'hydra_slideshow_plugin_display_slideshow_embed',
        'theme' => 'views_view',
      ),
    ),    
    'style' => array(
      'hydra_slideshow' => array(
        'title' => t('Hydra Slideshow'),
        'help' => t('Display the results as a slideshow.'),
        'handler' => 'hydra_slideshow_plugin_style_hydra_slideshow',
        'theme' => 'hydra_slideshow',
        'uses options' => TRUE,        
        'uses row plugin' => TRUE,
        'type' => 'normal',
        'parent' => 'default',
      ),
    ),
    'row' => array(
      'playlist_item' => array(
        'title' => t('Playlist item'),
        'help' => t('Display the single result as a playlist item.'),
        'handler' => 'hydra_slideshow_plugin_row_item_view',
        'theme' => 'hydra_slideshow_item',
        'uses fields' => TRUE,        
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
