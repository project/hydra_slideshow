<?php

/**
 * @file
 * Contains the list style plugin.
 */

/**
 * Style plugin to render each item in a slideshow.
 *
 * @ingroup views_style_plugins
 */
class hydra_slideshow_plugin_style_hydra_slideshow extends views_plugin_style_default {
  function option_definition() {
    $options = parent::option_definition();
    $options['slideshow_theme'] = array('default' => 'default');   
    $options['slide_duration'] = array('default' => 5);   
    $options['player_width'] = array('default' => 350);   
    $options['player_height'] = array('default' => 263);   
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['slideshow_theme'] = array(
      '#title' => t('Theme'),
      '#type' => 'select',
      '#default_value' => $this->options['slideshow_theme'],
      '#options' => _hydra_slideshow_get_themes(),
      '#description'   => t('The slideshow theme.'), 
    );
    $form['slide_duration'] = array(
      '#title' => t('Duration'),
      '#type' => 'textfield',
      '#default_value' => $this->options['slide_duration'],
      '#description' => t('The duration of each slide in seconds.'),
    );
    $form['player_width'] = array(
      '#title' => t('Player width'),
      '#type' => 'textfield',
      '#default_value' => $this->options['player_width'],
      '#description' => t('The width of the slideshow player.'),
    );    
    $form['player_height'] = array(
      '#title' => t('Player height'),
      '#type' => 'textfield',
      '#default_value' => $this->options['player_height'],
      '#description' => t('The height of the slideshow player.'),
    );        
  }
}
