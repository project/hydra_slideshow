<?php

function hydra_slideshow_views_default_views() {
  $view = new view;
  $view->name = 'hydra_slideshow';
  $view->description = 'Hydra Slideshow';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'teaser' => array(
      'label' => 'Teaser',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'teaser',
      'table' => 'node_revisions',
      'field' => 'teaser',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'created' => array(
      'order' => 'DESC',
      'granularity' => 'second',
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('items_per_page', 6);
  $handler->override_option('use_pager', 'mini');
  $handler->override_option('style_plugin', 'hydra_slideshow');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'slideshow_theme' => 'default',
    'slide_duration' => '5',
    'player_width' => '350',
    'player_height' => '263',
  ));
  $handler->override_option('row_plugin', 'playlist_item');
  $handler->override_option('row_options', array(
    'overlay' => array(),
  ));
  $handler = $view->new_display('slideshow', 'Slideshow', 'slideshow_1');
  $handler = $view->new_display('slideshow_embed', 'Slideshow embed', 'slideshow_embed_1');
  $handler->override_option('items_per_page', 20);
  $handler->override_option('style_options', array(
    'grouping' => '',
    'slideshow_theme' => 'default',
    'slide_duration' => '5',
    'player_width' => '360',
    'player_height' => '340',
  ));
  $handler->override_option('slideshow_header', '');
  $handler->override_option('slideshow_footer', '');
  $handler->override_option('slideshow_hint', 'Dimensions: 360x340');

  $views[$view->name] = $view;
  return $views;
}