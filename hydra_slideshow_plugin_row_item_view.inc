<?php

/**
 * @file
 * Contains the node view row style plugin.
 */

/**
 * Plugin which performs a node_view on the resulting object.
 *
 * Most of the code on this object is in the theme function.
 */
class hydra_slideshow_plugin_row_item_view extends views_plugin_row {
  function option_definition() {
    $options = parent::option_definition();
    $options['overlay'] = array('default' => array());
    return $options;
  }

  /**
   * Provide a form for setting options.
   */
  function options_form(&$form, &$form_state) {
    $options = array();
    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      $options["{$handler->table}_{$field}"] = $handler->ui_name();
    }

    if (empty($this->options['overlay'])) {
      $this->options['overlay'] = array();
    }

    $form['overlay'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Overlay information'),
      '#options' => $options,
      '#default_value' => $this->options['overlay'],
      '#description' => t('Choose which of these fields must be displayed in the overlay area.'),
    );
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit($form, &$form_state) {
    $form_state['values']['row_options']['overlay'] = array_filter($form_state['values']['row_options']['overlay']);
  }
}
